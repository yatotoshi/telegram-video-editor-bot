import os, sys
from resources import config
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext


TOKEN = config['token']
PORT = int(os.environ.get('PORT', '5000'))


def connect(connection_type, handler_list):
    updater = Updater(TOKEN, use_context=True)
    for handler_def in handler_list:
        updater.dispatcher.add_handler(handler_def)

    if connection_type == 'webhooks':
        updater.start_webhook(listen="0.0.0.0",
                              port=PORT,
                              url_path=TOKEN)
        print('webhooks listener:')
        updater.bot.set_webhook("https://tlg-video-editor.herokuapp.com/" + TOKEN)
    if connection_type == 'longpoll':
        updater.bot.delete_webhook()
        print('longpoll listener:')
        updater.start_polling()
    updater.idle()