from telegram.ext import Filters, CommandHandler, MessageHandler, ConversationHandler, CallbackQueryHandler
from resources import string_storage as strings, config, keyboards, states, indicators
from db_tools import DB
import re

caption_check = re.compile(r'^[\w\d., ]{,' + str(config['max_label_size']) + '}$')


def add_handler(update, context):
    update.callback_query.answer()
    update.callback_query.edit_message_text(
        text=strings['video ask'],
        reply_markup=keyboards['cancel action'])
    return states['ADDING_VIDEO']


def set_video(update, context):
    update.message.reply_text(
        strings['label ask'],
        reply_markup=keyboards['cancel action'])
    context.user_data['video_id'] = update.message.video.file_id
    context.user_data['video_unique_id'] = update.message.video.file_unique_id
    context.user_data['file_size'] = update.message.video.file_size
    return states['SETTING_LABEL']


def non_video_err(update, context):
    update.message.reply_text(
        strings['non-video error'],
        reply_markup=keyboards['cancel action'])
    return states['ADDING_VIDEO']


def set_label(update, context):
    caption = update.message.text
    if caption_check.match(caption) is None:
        update.message.reply_text(
            strings['non-label error'],
            reply_markup=keyboards['cancel action'])
        return states['SETTING_LABEL']
    video_id = context.user_data['video_id']
    video_unique_id = context.user_data['video_unique_id']
    file_size = context.user_data['file_size']
    user_id = update.effective_user['id']
    db = DB()
    if db.get_video(user_id=user_id, caption=caption) is None:
        db.save_video(user_id, video_id, video_unique_id, file_size, caption)
        update.message.reply_text(strings['action done'], reply_markup=keyboards['general actions'])
        return ConversationHandler.END
    else:
        update.message.reply_text(strings['same label error'])
        return states['SETTING_LABEL']
