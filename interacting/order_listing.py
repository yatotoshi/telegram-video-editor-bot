from telegram.ext import Filters, CommandHandler, MessageHandler, ConversationHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from resources import string_storage as strings, config, keyboards, buttons, states, indicators
from db_tools import DB

from tools import decode_sequence, humanify_todo


def list_handler(update, context):
    user_id = update.effective_user['id']
    db = DB()
    resp = db.list_orders(user_id)
    if len(resp) == 0:
        update.callback_query.answer()
        update.callback_query.edit_message_text(strings['no orders error'],
                                                reply_markup=keyboards['general actions'])
        return ConversationHandler.END

    button_list = []
    button_row = []
    i = 0
    line_size = 9
    for order in resp:
        if i % line_size == line_size - 1:
            button_list.append(button_row)
            button_row = []
        i += 1
        button_row.append(InlineKeyboardButton(text=str(i),
                             callback_data=indicators['INFO_SQ'] + '::' + str(order['id'])))
    button_list.append(button_row)

    keyboard = InlineKeyboardMarkup(button_list + [[buttons['cancel']]])

    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['order list'].format(len(resp)),
                                            reply_markup=keyboard)

    return states['INFORMING']


def info(update, context):
    user_id = update.effective_user['id']
    oid = update.callback_query.data.split('::')[1]
    db = DB()
    order = db.get_order(oid)
    video_list = db.list_video(user_id, removed=True)
    context.user_data['oid_to_remove'] = oid
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['order info'].format(
        humanify_todo(decode_sequence(order['commands']), video_list), str(order['queue'])),
        reply_markup=keyboards['order info options'])
    return states['REMOVING_ORDER']


def remove_handler(update, context):
    oid_to_remove = context.user_data['oid_to_remove']
    db = DB()
    db.remove_order(oid=oid_to_remove)
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['action done'], reply_markup=keyboards['general actions'])
    return ConversationHandler.END