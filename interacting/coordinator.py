from telegram.ext import Filters, CommandHandler, MessageHandler, ConversationHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from resources import string_storage as strings, config, keyboards, states, indicators
from db_tools import DB

import interacting.adding as adding
import interacting.editing as editing
import interacting.video_listing as video_listing
import interacting.order_listing as order_listing
import interacting.general as general

MAX_LABEL_SIZE = config['max_label_size']
TIMEOUT_LIMIT = config['timeout_limit']

adding_video_conversation = ConversationHandler(
    entry_points=[CallbackQueryHandler(adding.add_handler,
                                       pattern='^' + indicators['ADD_VIDEO'] + '$')],

    states={
        states['ADDING_VIDEO']: [
            MessageHandler(Filters.video, adding.set_video),
            MessageHandler(Filters.all, adding.non_video_err)
        ],
        states['SETTING_LABEL']: [
            MessageHandler(Filters.text, adding.set_label)
        ],

    },

    fallbacks = [CallbackQueryHandler(general.cancel_handler,
                                      pattern='^' + indicators['CANCEL'] + '$')],

    map_to_parent={
            ConversationHandler.END: states['SELECTING'],
            ConversationHandler.TIMEOUT: ConversationHandler.END
        },
    conversation_timeout=TIMEOUT_LIMIT
)


listing_video_conversation = ConversationHandler(
    entry_points=[CallbackQueryHandler(video_listing.list_handler,
                                       pattern='^' + indicators['LIST_VIDEOS'] + '$')],

    states={
        states['CONFIRMING']: [
            CallbackQueryHandler(video_listing.confirm,
                                 pattern='^' + indicators['CONFIRM_SQ'] + '::[\d\w]*$')
        ],
        states['REMOVING_VIDEO']: [
            CallbackQueryHandler(video_listing.remove_handler,
                                 pattern='^' + indicators['REMOVE'] + '$')
        ],

    },

    fallbacks = [CallbackQueryHandler(general.cancel_handler,
                                      pattern='^' + indicators['CANCEL'] + '$')],

    map_to_parent={
            ConversationHandler.END: states['SELECTING'],
            ConversationHandler.TIMEOUT: ConversationHandler.END
        },
    conversation_timeout=TIMEOUT_LIMIT
)

listing_orders_conversation = ConversationHandler(
    entry_points=[CallbackQueryHandler(order_listing.list_handler,
                                       pattern='^' + indicators['LIST_ORDERS'] + '$')],

    states={
        states['INFORMING']: [
            CallbackQueryHandler(order_listing.info,
                                 pattern='^' + indicators['INFO_SQ'] + '::[\d\w]*$')
        ],
        states['REMOVING_ORDER']: [
            CallbackQueryHandler(order_listing.remove_handler,
                                 pattern='^' + indicators['REMOVE'] + '$')
        ],

    },

    fallbacks = [CallbackQueryHandler(general.cancel_handler,
                                      pattern='^' + indicators['CANCEL'] + '$')],

    map_to_parent={
            ConversationHandler.END: states['SELECTING'],
            ConversationHandler.TIMEOUT: ConversationHandler.END
        },
    conversation_timeout=TIMEOUT_LIMIT
)

choosing_edit_conversation = ConversationHandler(
    entry_points=[CallbackQueryHandler(editing.left_video_ask_handler,
                                       pattern='^' + indicators['PLACE_ORDER'] + '$'),],

    states={
        states['CHOOSING_EDIT']: [
            CallbackQueryHandler(editing.edit_ask_handler,
                                 pattern='^' + indicators['EDIT_ASK_SQ'] + '::[\d\w]*$'),
        ],

        states['SEG_MAKING']: [
            CallbackQueryHandler(editing.right_video_ask_handler,
                                 pattern='^' + indicators['CONCATENATE'] + '$'),
            CallbackQueryHandler(editing.size_ask_handler,
                                 pattern='^' + indicators['RESIZE'] + '$'),
            CallbackQueryHandler(editing.format_ask_handler,
                                 pattern='^' + indicators['REFORMAT'] + '$'),
        ],

        states['CONFIRMING']: [
            CallbackQueryHandler(editing.confirm,
                                 pattern='^' + indicators['CONFIRM_SQ'] + '::[\d\w]*::[\d\w]*$'),
        ],
    },

    fallbacks = [
        CallbackQueryHandler(editing.place_order,
                             pattern='^' + indicators['TAKE_QUEUE'] + '$'),
        CallbackQueryHandler(general.cancel_handler,
                             pattern='^' + indicators['CANCEL'] + '$'),

    ],

    map_to_parent={
        ConversationHandler.END: states['SEG_MAKING'],
            ConversationHandler.TIMEOUT: ConversationHandler.END
    },
    conversation_timeout=TIMEOUT_LIMIT
)

editing_video_conversation = ConversationHandler(
    entry_points=[CallbackQueryHandler(editing.left_video_ask_handler,
                                       pattern='^' + indicators['PLACE_ORDER'] + '$'),],

    states={
        states['CHOOSING_EDIT']: [
            CallbackQueryHandler(editing.edit_ask_handler,
                                 pattern='^' + indicators['EDIT_ASK_SQ'] + '::[\d\w]*$'),
        ],

        states['SEG_MAKING']: [
            CallbackQueryHandler(editing.right_video_ask_handler,
                                 pattern='^' + indicators['CONCATENATE'] + '$'),
            CallbackQueryHandler(editing.size_ask_handler,
                                 pattern='^' + indicators['RESIZE'] + '$'),
            CallbackQueryHandler(editing.format_ask_handler,
                                 pattern='^' + indicators['REFORMAT'] + '$'),
            CallbackQueryHandler(editing.cut_ask_handler,
                                 pattern='^' + indicators['CUT'] + '$'),
        ],

        states['CONFIRMING']: [
            CallbackQueryHandler(editing.confirm,
                                 pattern='^' + indicators['CONFIRM_SQ'] + '::[\d\w]*::[\d\w]*$'),
            MessageHandler(Filters.text, editing.confirm_msg)
        ],
    },

    fallbacks = [CallbackQueryHandler(general.cancel_handler,
                                      pattern='^' + indicators['CANCEL'] + '$'),
                 CallbackQueryHandler(editing.place_order,
                                      pattern='^' + indicators['TAKE_QUEUE'] + '$'),
    ],

    map_to_parent={
            ConversationHandler.END: states['SELECTING'],
            ConversationHandler.TIMEOUT: ConversationHandler.END
    },
    conversation_timeout=TIMEOUT_LIMIT
)

general_conversation = ConversationHandler(
    entry_points=[CommandHandler('start', general.start_handler)],

    states={
        states['GENERAL']: [
            CallbackQueryHandler(general.start_handler,
                                 pattern='^' + str(ConversationHandler.END) + '$')
        ],
        states['SELECTING']: [
            adding_video_conversation,
            editing_video_conversation,
            listing_video_conversation,
            listing_orders_conversation,
        ],
        ConversationHandler.TIMEOUT: [
            CallbackQueryHandler(general.timeout_handler,
                                 pattern='^[\w\W]*$')
        ]
    },

    fallbacks = [CommandHandler('cancel', general.cancel_handler)],
    conversation_timeout=TIMEOUT_LIMIT
)

handler_list = [
    #CommandHandler(config['secret'], general.order_done),
    CommandHandler('help', general.help_handler),
    general_conversation,
    MessageHandler(Filters.all, general.default_handler)
]