from telegram.ext import Filters, CommandHandler, MessageHandler, ConversationHandler, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
import re
from resources import string_storage as strings, config, keyboards, buttons, states, indicators, editing_options
from db_tools import DB
from tools import *


def left_video_ask_handler(update, context):
    user_id = update.effective_user['id']
    db = DB()
    resp = db.list_video(user_id=user_id)
    if len(resp) == 0:
        update.callback_query.answer()
        update.callback_query.edit_message_text(strings['no video error'],
                                                reply_markup=keyboards['general actions'])
        return ConversationHandler.END
    context.user_data['video_list'] = resp
    button_list = [
        [ InlineKeyboardButton(text=video['caption'],
                               callback_data=indicators['EDIT_ASK_SQ'] + '::' + str(video['id'])) ]
        for video in resp]
    keyboard = InlineKeyboardMarkup(button_list + [[buttons['cancel']]])
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['left video ask'],
                                            reply_markup=keyboard)
    return states['CHOOSING_EDIT']


def edit_ask_handler(update, context):
    video_list = context.user_data['video_list']
    left_vid = update.callback_query.data.split('::')[1]

    if not (left_vid == ''):
        for video in video_list:
            if str(video['id']) == left_vid:
                left_video = video
                break
        context.user_data['left_video'] = left_video

        context.user_data['command'] = left_vid

    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['editing ask'],
                                            reply_markup=keyboards['editing actions'])
    return states['SEG_MAKING']


def right_video_ask_handler(update, context):
    resp = context.user_data['video_list']
    button_list = [
        [InlineKeyboardButton(text=video['caption'],
                              callback_data=indicators['CONFIRM_SQ'] + '::' +
                                            indicators['CONCATENATE'] + '::' +
                                            str(video['id']))]
        for video in resp]
    keyboard = InlineKeyboardMarkup(button_list + [[buttons['cancel']]])
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['right video ask'],
                                            reply_markup=keyboard)
    return states['CONFIRMING']


def size_ask_handler(update, context):
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['size ask'],
                                            reply_markup=keyboards['resize options'])
    return states['CONFIRMING']


def format_ask_handler(update, context):
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['right video ask'],
                                            reply_markup=keyboards['reformat options'])
    return states['CONFIRMING']


def cut_ask_handler(update, context):
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['cut ask'],
                                            reply_markup=keyboards['cancel action'])
    return states['CONFIRMING']


def confirm(update, context):
    video_list = context.user_data['video_list']
    left_video = context.user_data['left_video']
    prev_command = context.user_data['command']

    action = update.callback_query.data.split('::')[1]
    param = update.callback_query.data.split('::')[2]

    plus = editing_options['concatenation']
    resize = editing_options['resize']
    reform = editing_options['reformat']

    command = ''
    if action == indicators['CONCATENATE']:
        command = '(' + prev_command + ')' + plus + param
    elif action == indicators['RESIZE']:
        command = '(' + prev_command + ')' + resize + param
    elif action == indicators['REFORMAT']:
        command = '(' + prev_command + ')' + reform + param

    context.user_data['command'] = command

    update.callback_query.answer()
    update.callback_query.edit_message_text(
        strings['confirm format'].format(humanify_todo(decode_sequence(command), video_list)),
                                            reply_markup=keyboards['confirming'])
    return states['CHOOSING_EDIT']


def confirm_msg(update, context):
    msg = update.message.text
    video_list = context.user_data['video_list']
    prev_command = context.user_data['command']
    if not (re.match(r'^(?=.)([+-]?([0-9]*)(\.([0-9]+))?) (?=.)([+-]?([0-9]*)(\.([0-9]+))?)$', msg) is None):
        command = '(' + prev_command + ')' + editing_options['cut'] + msg.replace(' ', '_').replace('.', 'dot')
        context.user_data['command'] = command
        update.message.reply_text(
        strings['confirm format'].format(humanify_todo(decode_sequence(command), video_list)),
                                            reply_markup=keyboards['confirming'])
        return states['CHOOSING_EDIT']
    else:
        update.message.reply_text(strings['non-timing error'], reply_markup=keyboards['cancel action'])



def place_order(update, context):
    user_id = update.effective_user['id']
    commands = context.user_data['command']
    db = DB()
    queue_place = db.save_order(user_id, commands)
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['order placed'].format(queue_place),
                                            reply_markup=keyboards['general actions'])
    return ConversationHandler.END