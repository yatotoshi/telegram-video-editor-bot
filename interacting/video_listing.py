from telegram.ext import Filters, CommandHandler, MessageHandler, ConversationHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from resources import string_storage as strings, config, keyboards, buttons, states, indicators
from db_tools import DB


def list_handler(update, context):
    user_id = update.effective_user['id']
    db = DB()
    resp = db.list_video(user_id)
    if len(resp) == 0:
        update.callback_query.answer()
        update.callback_query.edit_message_text(strings['no video error'],
                                                reply_markup=keyboards['general actions'])
        return ConversationHandler.END

    context.user_data['video_list'] = resp

    button_list = [
        [InlineKeyboardButton(text=video['caption'],
                              callback_data=indicators['CONFIRM_SQ'] + '::' + str(video['id']))]
        for video in resp]
    keyboard = InlineKeyboardMarkup(button_list + [[buttons['cancel']]])

    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['video list'], reply_markup=keyboard)
    #context.user_data['start_over_flag'] = True

    return states['CONFIRMING']


def confirm(update, context):
    video_list = context.user_data['video_list']
    vid_to_remove = update.callback_query.data.split('::')[1]
    context.user_data['vid_to_remove'] = vid_to_remove
    caption = ''
    for video in video_list:
        if str(video['id']) == vid_to_remove:
            caption = video['caption']
            break

    keyboard = InlineKeyboardMarkup([
        [InlineKeyboardButton(text=strings['confirm button'],
                              callback_data=indicators['REMOVE'])],
        [buttons['cancel']],
    ])
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['remove video confirm'].format(caption),
                                            reply_markup=keyboard)
    return states['REMOVING_VIDEO']



def remove_handler(update, context):
    vid_to_remove = context.user_data['vid_to_remove']
    db = DB()
    db.remove_video(vid=vid_to_remove)
    update.callback_query.edit_message_text(strings['done'], reply_markup=keyboards['general actions'])
    return ConversationHandler.END