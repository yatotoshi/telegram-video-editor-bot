from telegram.ext import Filters, CommandHandler, MessageHandler, ConversationHandler, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from resources import string_storage as strings, config, keyboards, buttons, states, indicators, editing_options
from telegram import Bot
from tools import decode_sequence, humanify_todo
from db_tools import DB

def default_handler(update, context):
    if update.effective_user.id == config['ruler_id']:
        user_id, commands = update.message.caption.split(':::')
        db = DB()
        vl = db.list_video(user_id=user_id)
        is_document = False
        if not (update.message.document is None):
            is_document = True
            file_id = update.message.document.file_id
        else:
            file_id = update.message.video.file_id
        bot = Bot(config['token'])
        if is_document:
            bot.send_document(user_id, file_id,
                              caption=strings['order done'].format(
                                  humanify_todo(decode_sequence(commands), vl)
                              ))
        else:
            bot.send_video(user_id, file_id,
                              caption=strings['order done'].format(
                                  humanify_todo(decode_sequence(commands), vl)
                              ))
    else:
        context.user_data['start_over_flag'] = True
        start_handler(update, context)


def start_handler(update, context):
    if context.user_data.get('start_over_flag'):
        update.callback_query.answer()
        update.callback_query.edit_message_text(strings['greeting'],
                                                reply_markup=keyboards['general actions'])
    else:
        update.message.reply_text(strings['greeting'],
                                  reply_markup=keyboards['general actions'])
    context.user_data['start_over_flag'] = False
    return states['SELECTING']


def help_handler(update, context):
    update.message.reply_text(strings['help'])


def cancel_handler(update, context):
    update.callback_query.answer()
    update.callback_query.edit_message_text(strings['cancel video adding'],
                                            reply_markup=keyboards['general actions'])
    return ConversationHandler.END


def order_done(update, context):
    bot = Bot(config['token'])
    bot.send_document()


def timeout_handler(update, context):
    bot = Bot(config['token'])
    bot.send_message(chat_id=update.effective_chat['id'], text=strings['bye'])
    #update.message.reply_text(strings['bye'])
    return ConversationHandler.TIMEOUT