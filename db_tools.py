import psycopg2
import os
from resources import config

connection_type = 'HEROKU'
try:
    database_url = os.environ['DATABASE_URL']
except:
    connection_type = 'HOME'

conn = None
cursor = None

class DB:
    def __init__(self):
        db_conf = config['local_db_conf']
        self.__conn = None
        self.__cursor = None
        self.__db = db_conf

    def connect(self):
        db = self.__db
        if connection_type == 'HEROKU':
            self.__conn = psycopg2.connect(database_url, sslmode='require')
        else:
            self.__conn = psycopg2.connect("dbname='" + db['name'] + "' user='" + db['user'] +
                                           "' host='" + db['host'] + "' password='" + db['password'] + "'")

    def disconnect(self):
        self.__conn.commit()
        self.__cursor.close()
        self.__conn.close()

    def save_video(self, user_id, video_id, video_unique_id, file_size, caption='<NONE>'):
        values = [str(user_id), str(video_id), str(video_unique_id), file_size, str(caption)]
        params = ['user_id', 'video_id', 'video_unique_id', 'file_size', 'caption']
        values_pr = []
        for value in values:
            if type(value) is str:
                values_pr += ["'" + value + "'"]
            else:
                values_pr += [str(value)]

        values_str = ', '.join(values_pr)

        self.connect()
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('INSERT INTO videos(' + ', '.join(params) + ') VALUES(' + values_str + ')')
        self.disconnect()

    def get_video(self, vid=None, user_id=None, caption=None):
        self.connect()
        self.__cursor = self.__conn.cursor()
        comand = ''
        if (vid is None) and not(user_id is None) and not(caption is None):
            comand = 'SELECT id, user_id, video_id, video_unique_id, ' \
                     'file_size, caption, date ' \
                     'FROM videos ' \
                     'WHERE user_id = \'' + str(user_id) + '\' and caption = \'' + str(caption) + '\' '
        elif (user_id is None) and (caption is None) and not(vid is None):
            comand = 'SELECT id, user_id, video_id, video_unique_id, ' \
                     'file_size, caption, date ' \
                     'FROM videos ' \
                     'WHERE id = \'' + str(vid) + '\''
        else:
            raise Exception('DB error: get_video can\'t be called with these arguments:'
                            '\nvid = ' + str(vid) +
                            '\nuser_id = ' + str(user_id) +
                            '\ncaption = ' + str(caption))
        self.__cursor.execute(comand)
        data = self.__cursor.fetchone()
        self.disconnect()
        if data is None:
            return None
        row = list(data)
        data_tr = {
            'id': row[0],
            'user_id': row[1],
            'video_id': row[2],
            'video_unique_id': row[3],
            'file_size': row[4],
            'caption': row[5],
            'timestamp': row[6]
        }
        return data_tr

    def list_video(self, user_id=None, vids=None, removed=False):
        self.connect()
        self.__cursor = self.__conn.cursor()
        addition = ''
        if not removed:
            addition = ' and is_removed = FALSE'
        if not(user_id is None) and (vids is None):
            comand = 'SELECT id, user_id, video_id, video_unique_id, file_size, caption, date ' \
                     'FROM videos WHERE user_id = \'' + str(user_id) + '\'' + addition
        elif (user_id is None) and not(vids is None) and (type(vids) is list):
            ors = 'or '.join(['id = ' + str(vid) for vid in vids])
            # NOT TESTED #
            comand = 'SELECT id, user_id, video_id, video_unique_id, file_size, caption, date ' \
                     'FROM videos WHERE (' + ors + ') and is_removed = FALSE'
        else:
            raise Exception('DB error: list_video can\'t be called with these arguments:'
                            '\nuser_id = ' + str(user_id) +
                            '\nvids = ' + str(vids))
        self.__cursor.execute(comand)
        data = self.__cursor.fetchall()
        self.disconnect()
        data_tr = []
        for row in data:
            row = list(row)
            data_tr += [{
                'id': row[0],
                'user_id': row[1],
                'video_id': row[2],
                'video_unique_id': row[3],
                'file_size': row[4],
                'caption': row[5],
                'timestamp': row[6]
            }]
        return data_tr

    def save_order(self, user_id, commands):
        self.connect()
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('INSERT INTO orders(user_id, commands) '
                              'VALUES(\'' + str(user_id) + '\', \'' + str(commands) + '\')')
        self.__cursor.execute('SELECT COUNT(*) FROM orders')
        data = self.__cursor.fetchone()
        self.disconnect()
        return str(list(data)[0])

    def remove_video(self, vid=None, user_id=None, caption=None):
        self.connect()
        self.__cursor = self.__conn.cursor()
        if (vid is None) and not (user_id is None) and not (caption is None):
            command = 'UPDATE videos SET is_removed = TRUE ' \
                      'WHERE user_id = \'' + str(user_id) + '\' and caption = \'' + str(caption) + '\''
        elif (user_id is None) and (caption is None) and not (vid is None):
            command = 'UPDATE videos SET is_removed = TRUE ' \
                      'WHERE id = \'' + str(vid) + '\''
        else:
            raise Exception('DB error: get_video can\'t be called with these arguments:'
                            '\nvid = ' + str(vid) +
                            '\nuser_id = ' + str(user_id) +
                            '\ncaption = ' + str(caption))
        self.__cursor.execute(command)
        self.disconnect()

    def get_order(self, oid):
        if oid is None:
            return None
        self.connect()
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('SELECT id, user_id, commands '
                              'FROM orders '
                              'WHERE id = ' + str(oid))
        data = self.__cursor.fetchone()
        if data is None:
            return None

        self.__cursor.execute('SELECT COUNT(id) '
                              'FROM orders '
                              'WHERE id < ' + str(oid))
        queue = list(self.__cursor.fetchone())[0] + 1
        self.disconnect()
        rows = list(data)
        order = {
            'id': rows[0],
            'user_id': rows[1],
            'commands': rows[2],
            'queue': queue
        }
        return order


    def pop_order(self):
        self.connect()
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('SELECT MIN(id) FROM orders')
        data = self.__cursor.fetchone()
        oid = list(data)[0]
        if data is None:
            return None

        order_to_perform = self.get_order(oid)
        if order_to_perform is None:
            return None
        self.remove_order(oid)

        return order_to_perform


    def list_orders(self, user_id):
        self.connect()
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('SELECT id, user_id, commands '
                              'FROM orders '
                              'WHERE user_id = \'' + str(user_id) + '\'')
        data = self.__cursor.fetchall()
        self.disconnect()
        data_tr = []
        for row in data:
            row = list(row)
            data_tr += [{
                'id': row[0],
                'user_id': row[1],
                'commands': row[2]
            }]
        return data_tr

    def remove_order(self, oid):
        self.connect()
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('DELETE FROM orders '
                              'WHERE id = ' + str(oid))
        self.disconnect()

