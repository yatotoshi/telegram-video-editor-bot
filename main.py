import sys
from connect import connect
from interacting.coordinator import handler_list


connection_type = 'longpoll'
if (len(sys.argv) > 1) and (sys.argv[1] == 'HEROKU'):
    connection_type = 'webhooks'
connect(connection_type, handler_list)