from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler
import os

config = {
    'max_label_size': 128,
    'max_file_size': 50,
    'working_dir': '',
    'token': None,
    'secret': None,
    'ruler_id': None,
    'timeout_limit': None,
    'owner_id': None,
    'local_db_conf': {
        'name': None,
        'user': None,
        'host': None,
        'password': None
    },
    'app': {
        'api_id': None,
        'api_hash': None,
        'phone': None
    }
}


string_storage = {
    'greeting': 'Этот бот позволит вам отредактировать видео '
                '- измененить разрешение, склеить несколько клипов в один, '
                'сменить формат и обрезать загруженное видео.',

    'help': 'Чтобы начать работу, вам нужно загрузить видео. '
            'Нажмите кнопку "Добавить видео" и следуйте инструкциям. '
            'Чтобы запросить обработку нажмите кнопку "Начать редактирование".',

    'error': 'Не могу распознать вашу команду. Может быть, вам нужен /help?',

    'video ask': 'Хорошо, теперь загрузите видео или перешлите его из другого'
                 ' диалога.',

    'label ask': 'Готово!\n'
                 'Теперь введите название этого видео:',

    'non-video error': 'Не похоже на видео, попробуйте еще раз.',

    'too big file error': 'Это видео слишком большое, я поддерживаю только'
                          'видео весом менее 50мб.',

    'non-label error': 'В качестве метки можно использовать только текст '
                       'длиной до {0} символов. Пожалуйста, не используйте в '
                       'метке эмодзи, переносы строк и специальные символы.\n'
                       'Попробуйте еще раз:',

    'same label error': 'У вас уже есть видео с таким названием :(\n'
                        'Попробуйте еще раз:',

    'action done': 'Отлично, что дальше?',

    'cancel video adding': 'Хорошо, что-нибудь еще?',

    'add video button': 'Добавить видео',

    'list video button': 'Вывести список моих видео',

    'place order button': 'Начать редактирование',

    'list order button': 'Показать мои заявки',

    'cancel button': 'Отмена',

    'back button': 'Вернуться в меню',

    'remove button': 'Удалить',

    'done button': 'Готово',

    'continue button': 'Продолжить обработку',

    'confirm button':  'Подтвердить',

    'no video error': 'Вы еще не загружали видео. Хотите добавить?',

    'no orders error': 'У вас нет заявок.\nЧтобы запросить обработку, '
                       'добавьте видео и нажмите кнопку "Начать редактирование".',

    'video list': 'Список ваших видео (нажмите на на видео, чтобы удалить):',

    'order list': 'У вас {0} обработок в очереди, нажмите на номер, '
                  'чтобы получить дополнительную информацию.',

    'editing ask': 'Выберите преобразование:',

    'concatenation button': 'Соединить',

    'resize button': 'Изменить разрешение',

    'reformat button': 'Сменить формат (mp4, webm)',

    'cut button': 'Обрезать',

    'left video ask': 'Выберите видео для обработки:',

    'right video ask': 'Выберите второе видео:',

    'size ask': 'Выберите разрешение (ширина в пикселях):',

    'cut ask': 'Введите 2 числа через пробел. Первое число - '
               'секунда, с которой будет обрезано видео (введите 0,'
               'чтобы выбрать начало видеоклипа) а второе - секунда, '
               'до которой будет обрезано видео (введите 0, чтобы '
               'выбрать конец видеоклипа).\n'
               'Например, комманда "0 4.45" вернет фрагмент видео с его '
               'начала до 4.45 секунды.',

    'non-timing error': 'Возможно, вы ошиблись при вводе, попробуйте еще раз:',

    'confirm format': 'Будет выполнено редактирование:\n{0}\n'
                      'Нажмите на кнопку "Продолжить обработку", чтобы выполнить '
                      'преобразования результата этой обработки.',

    'order placed': 'Ваш запрос будет обработан в ближайшее время, '
                    'ваше место в очереди: {0}. Что-нибудь еще?',

    'remove video confirm': 'Вы действительно хотите удалить видео "{0}"?',

    'order info': 'Заказ: \n{}\nМесто в очереди: {}.',

    'done': 'Готово. Что-нибудь еще?',

    'order done': 'Ваша просьба\n{0}\nОбработана. Чтобы снова вызвать меню, '
                  'напишите /start в этот диалог.',

    'bye': 'К сожалению, время ожидания истекло. Если вам что-нибудь '
           'понадобится, просто напишите /start.'
}

_state_list = [
    'GENERAL', 'SELECTING',
    'ADDING_VIDEO', 'SETTING_LABEL',    # adding video
    'CHOOSING_EDIT', 'CONFIRMING', 'FINISH', 'REMOVING_VIDEO', 'REMOVING_ORDER',
    'GETTING_QUEUE', 'SAVE_SEG', 'LEFT_VIDEO_ASKING', 'SEG_MAKING', 'INFORMING',
    'RIGHT_VIDEO_ASKING',    # concatenating
]
states = {}

_indicator_list = ['CANCEL', 'BACK',
                   'ADD_VIDEO', 'LIST_VIDEOS', 'PLACE_ORDER_SQ', 'LIST_ORDERS',
                   'LEFT_VIDEO_ASK', 'MAKE_SEG', 'EDIT_ASK_SQ', 'CUT',
                   'CONCATENATE', 'RESIZE', 'REFORMAT', 'CONFIRM_SQ', 'CONTINUE',
                   'VIDEO_SQ', 'PLACE_ORDER', 'TAKE_QUEUE', 'REMOVE', 'INFO_SQ']
indicators = {}


def _init():
    i = 0
    for key in _state_list:
        states[key] = str(i)
        i += 1

    for key in _indicator_list:
        indicators[key] = str(i)
        i += 1
    config['working_dir'] = os.path.join(os.path.abspath(os.path.curdir).replace('backend', ''), 'work_dir') + '/'
    try:
        config['token'] = os.environ['token']
        config['secret'] = os.environ['secret']
        config['ruler_id'] = int(os.environ['ruler_id'])
        config['timeout_limit'] = int(os.environ['timeout_limit'])
        config['owner_id'] = int(os.environ['owner_id'])

        config['app']['api_id'] = int(os.environ['app_api_id'])
        config['app']['api_hash'] = os.environ['app_api_hash']
        config['app']['phone'] = os.environ['app_phone']
    except:
        from local_conf import secs
        config['token'] = secs['token']
        config['secret'] = secs['secret']
        config['ruler_id'] = secs['ruler_id']
        config['timeout_limit'] = secs['timeout_limit']
        config['owner_id'] = secs['owner_id']
        config['local_db_conf'] = secs['local_db_conf']
        config['app'] = secs['app']

_init()

editing_formats = ['mp4', 'webm']
editing_reducers = ['240p', '360p', '480p', '720p']
editing_options = {
    'concatenation': '+',
    'resize': '.resize_',
    'reformat': '.reformat_',
    'cut': '.cut_'
}

buttons = {
    'back': InlineKeyboardButton(text=string_storage['back button'],
                              callback_data=str(ConversationHandler.END)),

    'cancel': InlineKeyboardButton(text=string_storage['cancel button'],
                              callback_data=indicators['CANCEL']),
}

keyboards = {
    'general actions': InlineKeyboardMarkup([
        [InlineKeyboardButton(text=string_storage['add video button'],
                              callback_data=indicators['ADD_VIDEO']),
        InlineKeyboardButton(text=string_storage['list video button'],
                             callback_data=indicators['LIST_VIDEOS'])],
        [InlineKeyboardButton(text=string_storage['place order button'],
                              callback_data=indicators['PLACE_ORDER']),
         InlineKeyboardButton(text=string_storage['list order button'],
                              callback_data=indicators['LIST_ORDERS'])]
    ]),

    'cancel action': InlineKeyboardMarkup([[buttons['cancel']]]),

    'back action': InlineKeyboardMarkup([[buttons['back']]]),

    'editing actions': InlineKeyboardMarkup([
        [InlineKeyboardButton(text=string_storage['concatenation button'],
                              callback_data=indicators['CONCATENATE'])],
        [InlineKeyboardButton(text=string_storage['resize button'],
                              callback_data=indicators['RESIZE'])],
        [InlineKeyboardButton(text=string_storage['reformat button'],
                              callback_data=indicators['REFORMAT'])],
        [InlineKeyboardButton(text=string_storage['cut button'],
                              callback_data=indicators['CUT'])],
        [buttons['cancel']]
    ]),

    'resize options': InlineKeyboardMarkup([[InlineKeyboardButton(text=option,
                              callback_data=indicators['CONFIRM_SQ'] + '::' +
                                            indicators['RESIZE'] + '::' + option)]
     for option in editing_reducers] + [[buttons['cancel']]
    ]),

    'reformat options': InlineKeyboardMarkup([[InlineKeyboardButton(text=option,
                              callback_data=indicators['CONFIRM_SQ'] + '::' +
                                            indicators['REFORMAT'] + '::' + option)]
     for option in editing_formats] + [[buttons['cancel']]
    ]),

    'confirming': InlineKeyboardMarkup([
        [InlineKeyboardButton(text=string_storage['continue button'],
                              callback_data=indicators['EDIT_ASK_SQ'] + '::')],
        [InlineKeyboardButton(text=string_storage['done button'],
                              callback_data=indicators['TAKE_QUEUE'])],
        [buttons['cancel']]
    ]),

    'order info options': InlineKeyboardMarkup([
        [InlineKeyboardButton(text=string_storage['remove button'],
                              callback_data=indicators['REMOVE'])],
        [buttons['cancel']]
    ])
}

ffmpeg_keys = {
    'to_ts': [
        '-i', '{params[0]}', '-c', 'copy', '-bsf:v',
        'h264_mp4toannexb', '-f', 'mpegts', '{params[1]}'
    ],

    'sound_check': ['-i', '{params[0]}', '-show_streams',
                    '-select_streams', 'a', '-loglevel', 'error'],

    'add_silent': ['-f', 'lavfi', '-i', 'aevalsrc=0', '-i', '{params[0]}',
                   '-c:v', 'copy', '-c:a', 'aac', '-map', '0',
                   '-map', '1:v', '-shortest', '{params[1]}'],

    'concatenating': [
        '-i', '"concat:{params[0]}|{params[1]}"', '-c',
        'copy', '', '-bsf:a', 'aac_adtstoasc', '{params[2]}'
    ],

    'conc_test': ['-i', '{params[0]}', '-i',
                  '{params[1]}', '-filter_complex',
                  '"[0:v][0:a][1:v][1:a]concat=n=2:v=1:a=1[v][a]"', '-map',
                  '"[v]"', '-map', '"[a]"', '{params[2]}']
}

print('resources were inited')

