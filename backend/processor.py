from db_tools import DB
import tools
import os
from resources import editing_options, config
import telegram
from backend.ffmpeg_tools import name, process_video
import backend.telegram_tools as tt

WORKING_DIR = config['working_dir']


def download_videos(order):
    def find_video(vid, vl):
        for video in vl:
            if str(video['id']) == str(vid):
                return video
        return None

    user_id = order['user_id']
    db = DB()
    video_list = db.list_video(user_id, removed=True)
    todo = tools.decode_sequence(order['commands'])
    com, param = todo[0]
    to_download = [(find_video(param, video_list)['video_id'], param)]
    for line in todo[1:]:
        com, param = line
        if com == editing_options['concatenation']:
            to_download.append((find_video(param, video_list)['video_id'], param))
    print('to download list formed: ' + str(to_download))
    bot = telegram.Bot(config['token'])
    for dwn in to_download:
        file_id, vid = dwn
        if os.path.exists(WORKING_DIR + name(vid)):
            continue
        file = bot.get_file(file_id)
        file.download(WORKING_DIR + name(vid))
        print('file ' + str(file_id) + ' downloaded')



def main():
    db = DB()
    order = db.pop_order()
    while not (order is None):
        print('order to perform: ' + str(order))
        download_videos(order)
        file_to_send = process_video(tools.decode_sequence(order['commands']))
        tt.send(file_to_send, order['user_id'], order['commands'])
        order = db.pop_order()
    print('all orders are processed!')
    return 0

main()