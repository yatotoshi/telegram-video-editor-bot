from telethon import TelegramClient, sync, utils, errors
from telethon.tl.types import DocumentAttributeAudio
import mimetypes
from resources import config
import os

WORKING_DIR = config['working_dir']


def send(file, user_id, commands):
    entity = 'video_editor'  # имя сессии - все равно какое
    api_id = config['app']['api_id']
    api_hash = config['app']['api_hash']
    phone = config['app']['phone']
    client = TelegramClient(entity, api_id, api_hash)
    client.connect()
    if not client.is_user_authorized():
        # client.send_code_request(phone) #при первом запуске - раскомментить, после авторизации для избежания FloodWait советую закомментить
        client.sign_in(phone, input('Enter code: '))
    client.start()
    print('upload bot is set')

    mimetypes.add_type('video/mp4','.mp4')
    mimetypes.add_type('video/webm','.webm')
    print('uploading is started')
    msg = client.send_file(str('video_edtor_bot'),
                           WORKING_DIR + file,
                           caption=str(user_id) + ':::' + str(commands),
                           force_document=True,
                           allow_cache=False)
    print('file uploaded')
    client.disconnect()
    return msg