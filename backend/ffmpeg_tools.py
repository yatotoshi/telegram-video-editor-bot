import subprocess
import moviepy.editor as mp
import os
from resources import config, editing_options

from random import randint as random

plus = editing_options['concatenation']
resize = editing_options['resize']
reform = editing_options['reformat']
cut = editing_options['cut']

WORKING_DIR = config['working_dir']


def safe_remove(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)
        print(file_path + ' deleted')
        return True
    else:
        print('file ' + file_path + 'doesn\'t exist')
        return False


def name(vid, ext='mp4'):
    return 'vid_' + str(vid) + '.' + ext


def choose_name(src, ext='mp4'):
    num = str(random(1000, 9999))
    output_name = name(num, ext)
    while src.find(num) != -1:
        num = str(random(1000, 9999))
        output_name = name(num, ext)
    return output_name

def process_video(todo):
    command, param = todo[0]
    result = name(param)
    for line in todo[1:]:
        command, param = line
        old_res = result
        if command == plus:
            result = concatenate(result, name(param))
            safe_remove(WORKING_DIR + name(param))
            safe_remove(WORKING_DIR + old_res)
            continue
        if command == resize:
            result = resize_by_width(result, param[:-1])
            safe_remove(WORKING_DIR + old_res)
            continue
        if command == reform:
            result = reformat(result, param)
            safe_remove(WORKING_DIR + old_res)
        if command == cut:
            result = make_cut(result, param)
        print('command ' + str(command) + ', ' + str(param) + ' is done')
    return result


def concatenate(source, right):
    output_name = choose_name(source)
    video_1 = mp.VideoFileClip(WORKING_DIR + source)
    video_2 = mp.VideoFileClip(WORKING_DIR + right)
    final_video = mp.concatenate_videoclips([video_1, video_2], method="compose")
    final_video.write_videofile(WORKING_DIR + output_name)
    return output_name


def resize_by_width(source, resol):
    output_name = choose_name(source)
    video_1 = mp.VideoFileClip(WORKING_DIR + source)
    clip_resized = video_1.resize(width=int(resol))
    clip_resized.write_videofile(WORKING_DIR + output_name)
    return output_name


def reformat(source, ext):
    video_1 = mp.VideoFileClip(WORKING_DIR + source)
    if ext == 'webm':
        output_name = choose_name(source, 'webm')
        video_1.write_videofile(WORKING_DIR + output_name,
                                codec='libvpx',
                                bitrate='500k')
    elif ext == 'gif':
        output_name = choose_name(source, 'gif')
        video_1.write_gif(WORKING_DIR + output_name, )
    else:
        output_name = choose_name(source, 'mp4')
        os.rename(WORKING_DIR + source, WORKING_DIR + output_name)
        f = open(WORKING_DIR + source, 'w')
        f.write('doesn\'t matter')
        f.close()
    return output_name


def make_cut(source, cut_s):
    video_1 = mp.VideoFileClip(WORKING_DIR + source)
    output_name = choose_name(source)
    start, end = cut_s.replace('dot', '.').split('_')
    start = float(start)
    end = float(end)

    if video_1.duration < start:
        start = 0
    if video_1.duration < end:
        end = 0

    if end == 0:
        video_1 = video_1.subclip(start)
    else:
        video_1 = video_1.subclip(start, end)
    video_1.write_videofile(WORKING_DIR + output_name)
    return output_name