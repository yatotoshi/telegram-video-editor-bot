import re
from resources import editing_options

plus = editing_options['concatenation']
resize = editing_options['resize']
reform = editing_options['reformat']
cut = editing_options['cut']
_fin_re = re.compile('^[\d]*$')

def decode_sequence(sequence):
    def unpack(pack):
        if _fin_re.match(pack):
            return False, pack
        else:
            brc = pack.rfind(')')
            command = pack[brc+1:]
            return pack[1:brc], command

    def check(s):
        counter = 0
        for c in s:
            if c == '(':
                counter += 1
            if c == ')':
                counter -= 1
            if counter < 0:
                return False
        return counter == 0

    if not check(sequence):
        raise Exception('Invalid command sequence: ' + sequence)
    todo_list = []
    while sequence:
        sequence, command = unpack(sequence)
        todo_list += [command]
    todo_list.reverse()


    lines = []

    start_vid = todo_list[0]
    for command in todo_list[1:]:
        if command.find(plus) != -1:
            lines.append((plus, command[len(plus):]))
        if command.find(resize) != -1:
            lines.append((resize, command[len(resize):]))
        if command.find(reform) != -1:
            lines.append((reform, command[len(reform):]))
        if command.find(cut) != -1:
            lines.append((cut, command[len(cut):]))

    parsed_td = []
    reform_param = ''
    resize_param = ''
    for line in lines:
        command, param = line
        if command == reform:
            reform_param = param
        #elif command == resize:
        #    resize_param = param
        else:
            parsed_td.append((command, param))

    if resize_param != '': parsed_td.append((resize, resize_param))
    if reform_param != '': parsed_td.append((reform, reform_param))
    return [('init', start_vid)] + parsed_td


def humanify_todo(todo, vl):
    def get_caption(vid, video_list):
        for video in video_list:
            if str(vid) == str(video['id']):
                return '"' + video['caption'] + '"'
    command, param = todo[0]
    lines = ['Взять видео ' + get_caption(param, vl)]
    for line in todo[1:]:
        cmd, param = line
        if cmd == plus:
            lines += ['соеденить с ' + get_caption(param, vl)]
        if cmd == resize:
            lines += ['сменить разрешение на ' + param]
        if cmd == reform:
            lines += ['сменить формат на ' + param]
        if cmd == cut:
            start, end = param.replace('dot', '.').split('_')
            start, end = start + 's', end + 's'
            if start == '0s':
                start = 'начала'
            if end == '0s':
                end = 'конца'
            lines += ['обрезать с {0} до {1}'.format(start, end)]
    return ',\n'.join(lines) + '.'