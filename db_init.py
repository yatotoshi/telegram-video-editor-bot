import psycopg2
import os
from resources import config

db_conf = config['local_db_conf']

connection_type = 'HEROKU'
try:
    database_url = os.environ['DATABASE_URL']
except:
    connection_type = 'HOME'

if connection_type == 'HEROKU':
    conn = psycopg2.connect(database_url, sslmode='require')
else:
    conn = psycopg2.connect("dbname='" + db_conf['name'] + "' user='" + db_conf['user'] +
                                   "' host='" + db_conf['host'] + "' password='" + db_conf['password'] + "'")



#conn = psycopg2.connect(connect_str)
cursor = conn.cursor()

cursor.execute("""DROP TABLE videos""")
cursor.execute("""DROP TABLE orders""")
cursor.execute("""DROP TABLE privileges""")

cursor.execute("""CREATE TABLE videos (
                id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
                user_id varchar(128), 
                video_id varchar(128), 
                video_unique_id varchar(128), 
                file_size int, 
                caption varchar(128),
                date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                is_removed boolean DEFAULT FALSE);""")

cursor.execute("""CREATE TABLE orders (
                id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
                user_id varchar(128), 
                commands varchar(512),
                date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP);""")

cursor.execute("""CREATE TABLE privileges (
                user_id varchar(128) PRIMARY KEY, 
                flags smallint DEFAULT 0 NOT NULL);""")


conn.commit()
cursor.execute("""SELECT * from orders""")

rows = cursor.fetchall()
print(rows)
cursor.close()
conn.close()
